import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
//import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NgxSpinnerService } from "ngx-spinner";

import * as moment from 'moment';


@Component({
  selector: 'app-appointment-details',
  templateUrl: './appointment-details.component.html',
  styleUrls: ['./appointment-details.component.css']
})
export class AppointmentDetailsComponent implements OnInit {
  filtersLoaded: Promise<boolean>;  
  appointmentForm:any=FormGroup;
  basicForm:any= FormGroup;
  IsSubmitted:any=false;
  
  staffs:any=[];
  
  cutomer_services:any=[];
  
  stepMinute:any;
  
  minDate:any=new Date()
  
  service_name:any='';

  btn_disable:boolean=false;

  keyword = 'name';
  
  formatter:any;
  
  clients:any=[];

  showCustomer:boolean=true;

  is_duplicate:boolean=false;
  

  custom_durations=[1,2,3,4,5,6,7,8,9,10,11,12];

  show_customer:boolean=true;  

  recurring_types :any=[{key:'N',value:'Does Not Repeat'},{key:'D',value:'Every Working Day'},{key:'C',value:'Custom'}];

  custom_recurring_types=[{key:'D',value:'Day'},{key:'W',value:'Week'},{key:'M',value:'Month'}];

  constructor(private router: Router,private commonservice: CommonService,
  
  private formBuilder: FormBuilder, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {

  this.Loading();
   
        
   
  }

submitData()
  {
      this.IsSubmitted=true;
     
      if (this.appointmentForm.status == "VALID") {
          
      this.spinner.show(); 
      
      this.appointmentForm.value.start_time=moment(this.appointmentForm.value.start_time).format('YYYY-MM-DD HH:mm:ss')
       
      let selectedMoment=this.appointmentForm.value.start_time;
      
      this.commonservice.postData(this.appointmentForm.value, "appointment/add").subscribe(res => {
          
      this.spinner.hide();
        
         
      if (res.status) {
          
      this.commonservice.showSuccess(res.message, "Book Appointment");

      
      this.appointmentForm.reset(); 
      
      this.IsSubmitted=false;
      
      this.appointmentForm.patchValue({staff_id:null,is_repeat:0}); 
      
      
      this.commonservice.newAppointment.next(res.data);
      
      this.commonservice.afterAppointment.next(res.customer_id);
      
      this.service_name="";
      
      this.commonservice.setDuration.next('');
      
      this.commonservice.setNav.next(1);
      
      }
      else{
        let errors=res.error;
        
        for (let error of errors) 
        {
         this.commonservice.showError(error, "Book Appointment")
        }
       }
      },
        err => {
            this.spinner.hide(); 
            if(err.status==401)
            {
             this.router.navigate(['/unauthorized']); 
            }  
            this.commonservice.showError(err.message, "Calender")    

        });           
      }
      
      
  }
  async fetchStaffs(){
  let body={};  
  if(!this.is_duplicate) 
  {
    body={staff_id:this.appointmentForm.value.staff_id};    
  }
  this.commonservice.postDataAsync(body, "dailyStaff").then(res => {
        
  if (res.status) {
      
  this.staffs=res.data
  
  }
  },
  err => {
      this.spinner.hide();
      if(err.status==401)
      {
       this.router.navigate(['/unauthorized']); 
      }  
      this.commonservice.showError(err.message, "Calender")    

  });                 
  }
  
  async fetchServices(){
   
  this.commonservice.postDataAsync("", "service/listing").then(res => {
      
  if (res.status) {
      
  this.cutomer_services=res.data.data;
 
  if(this.appointmentForm.value.id=='' || this.appointmentForm.value.id==null)
  {
  let result=this.cutomer_services.find(item => item.minutes === this.appointmentForm.value.duration.toString());
  
    if(typeof result != 'undefined')
    {
        this.appointmentForm.patchValue({customerservice_id:result.id});
    }
    else{
      this.appointmentForm.patchValue({customerservice_id:""});
    }
  }
  }
  },
  err => {
      if(err.status==401)
      {
       this.router.navigate(['/unauthorized']); 
      }  
      this.commonservice.showError(err.message, "Calender")    

  });                 
  }
  selectService(customerservice_id: any):void{
    let item=this.cutomer_services.find(res => res.id === parseInt(customerservice_id));
  //if(this.service_name=='')    
  //{
    this.appointmentForm.patchValue({service_name:item.name,duration:item.minutes});     
  //} 
  }
  selectCustomer(item: any):void{
  
  if(typeof item.c_id =='undefined')
  {
     let body={name:item.name,primary_contact_name:'',mobile:'',alternate_name:'',alternate_mobile:'',address:'',customer_notes:''};
     this.appointmentForm.patchValue(body);    
  }
  else{
    this.appointmentForm.patchValue(item);   
  }
   
  }
  
  changeRecurring()
  {
    if(this.appointmentForm.value.is_repeat=='N'){
    this.appointmentForm.get('no_term').clearValidators();
    }
    else{
      this.appointmentForm.get('no_term').setValidators([
        Validators.required
    ]);
    }
      if(this.appointmentForm.value.is_repeat!='C')
      {
          this.appointmentForm.patchValue({
              custom_type:'',custom_duration:''
          });
        this.appointmentForm.get('custom_type').clearValidators();
        this.appointmentForm.get('custom_duration').clearValidators();
      }
      
      else{
          this.appointmentForm.get('custom_type').setValidators([
                Validators.required
            ]);
            this.appointmentForm.get('custom_duration').setValidators([
                Validators.required
            ]);
            

      }
      this.appointmentForm.get('custom_type').updateValueAndValidity();
      this.appointmentForm.get('custom_duration').updateValueAndValidity();
      this.changerecurringOpt();
  }
 public myFilter = (d: Date): boolean => {
        const day = d.getDay();
        // Prevent Saturday and Sunday from being selected.
        return day !== 0 && day !== 6;
    }
   async Loading()
   {
      this.spinner.show();
      await this.initForm();
      await this.fetchServices();
      await this.fetchStaffs();
      await this.fetchSetting();
      await this.allCustomers();
      this.spinner.hide();
      
      this.filtersLoaded = Promise.resolve(true);
   }
   
   async fetchSetting()
   {
       this.commonservice.postDataAsync("", "setting/view").then(res => {
           

       if (res.status) {

       this.stepMinute=res.data.timepicker_interval;


       }
       },
       err => {
           this.spinner.hide();
           if(err.status==401)
           {
            this.router.navigate(['/unauthorized']); 
           }  
           this.commonservice.showError(err.message, "Calender")    

       });             
   } 
   async initForm()
   {
      this.commonservice.presentCustomer.subscribe(client => {
      this.appointmentForm =  this.formBuilder.group({
      id: [""],
      staff_id: ['',Validators.required],
      customer_id: [null],
      customerservice_id:[''],
      service_name:[''],
      duration:['',Validators.required],
      start_time:['', Validators.required],
      is_repeat:['N'],
      custom_type:[''],
      custom_duration:[''],
      no_term:[''],
      notes:[''],
      job_completion:[''],
      extra_materials:[''],
      summary:[''],
      name:[''],
      primary_contact_name:[''],
      mobile:[''],
      alternate_name:[''],
      alternate_mobile:[''],
      address:[''],
      customer_notes:[''],
      estimate:[]
    }) 
    this.appointmentForm.patchValue(client); 
    this.appointmentForm.patchValue({customer_id:client.id}); 
    this.appointmentForm.patchValue({id:""});
	if(this.appointmentForm.value.staff_id=='')
	{
		this.appointmentForm.patchValue({staff_id:null});
	}

    this.commonservice.setDuration.subscribe(body => {
     if(body!='')    
     {
      
      if(body.is_repeat!='N' && typeof body.is_repeat != 'undefined'){
        this.appointmentForm.get('no_term').setValidators([
          Validators.required
          ]);
          this.appointmentForm.get('no_term').updateValueAndValidity();
        }
        
        
          
         let current_date=new Date();
        
         if(current_date>body.start_time)
         {
             this.btn_disable=true;
         }
         this.service_name= body.service_name;
         if(body.staff_id!='')
         {
             body.staff_id=parseInt(body.staff_id);
         }
		     if(body.is_duplicate)
         {
           this.is_duplicate=body.is_duplicate;
           body.staff_id=parseInt(body.select_staff_id);
         }
         this.appointmentForm.patchValue(body);
         this.changerecurringOpt(); 
     }    
     })
  
    
    
   });
   
   }
   async allCustomers()
{
       
       this.commonservice.postDataAsync("", "dailyCustomers").then(res => {
        if (res.status) {
        this.clients=res.data.data;
        }
      },
        err => {
            this.spinner.hide();
            if(err.status==401)
            {
             this.router.navigate(['/unauthorized']); 
            }  
            this.commonservice.showError(err.message, "Dashboard")    

        }); 
 }
 selectEvent(item: any) {
  
  
    // do something with selected item
  }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  onFocused(e: any){
    // do something when input is focused
  }  
  changerecurringOpt()
  {
       
       this.recurring_types=[
        {key:'N',value:'Does Not Repeat'},
        {key:'D',value:'Every Working Day'},
        {key:moment(this.appointmentForm.value.start_time).format('dddd'),value:'Weekly on '+moment(this.appointmentForm.value.start_time).format('dddd')},
        {key:moment(this.appointmentForm.value.start_time).format('DD'),value:'Monthly on '+moment(this.appointmentForm.value.start_time).format('Do')},
        {key:'C',value:'Custom'} 
       ];
       
   
  } 
  
 changeDate()
 {
       let is_repeat=this.appointmentForm.value.is_repeat;
       
       if(is_repeat=='D' || is_repeat=='C' || is_repeat=='N')
       {
           
       }
       else{
         this.appointmentForm.patchValue({is_repeat:'N'});  
       } 
 } 
}
