import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPassword!:FormGroup;
  constructor() { }

  ngOnInit(): void {
    this.forgotPassword = new FormGroup({
      userEmail: new FormControl(null)
    })
  }

  forgotFormSubmit()
  {
    console.log(this.forgotPassword.value)
  }

}
