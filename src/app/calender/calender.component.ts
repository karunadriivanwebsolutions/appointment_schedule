import { Component, OnInit,ViewChild, AfterViewInit  } from '@angular/core';
import { CalendarOptions,FullCalendarComponent } from '@fullcalendar/angular'; // useful for typechecking
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { CommonService } from '../service/common.service';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { NgxSpinnerService } from "ngx-spinner";
import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit {
    innerWidth:any=window.innerWidth;
    userDetails:any;
    selectedStaff:any='';
    selectedMoment:any='';
    calHeading:any='';
    calSetting:any;
    initialView:any='';
    staff_id:any='';
    staffs:any=[];
    events:any;
    start_date:any='';
    index:any=0;
    service_name:any='';
    reschedule_id:any='';
    calendermodes:any=[{text:'Monthly',value:'dayGridMonth'},{text:'Weekly',value:'timeGridWeek'},{text:'Daily',value:'resourceTimeGrid'}];
    
    datepicker_open:boolean=false;
    appointmentDtl:any;
    contentHeight:any=(window.innerHeight*80)/100
    calendarOptions: CalendarOptions = {
    schedulerLicenseKey:'0091685131-fcs-1639845459',
    datesAboveResources:false,
    editable: false,
    selectable: false,
    headerToolbar:false,
    allDaySlot:false,
    displayEventTime:false,
    eventOverlap:true,
    contentHeight:this.contentHeight,
    nowIndicator:true,
    dayHeaderFormat:{
       weekday: 'short', month: 'short', day: 'numeric', omitCommas: false
    },
    slotLabelFormat:{
    hour: 'numeric',
    minute: '2-digit',
    omitZeroMinute: true,

    },
	events:[],
    resourceOrder:'sort_order',
    eventResize:this.updateEvent.bind(this),
    eventDrop:this.updateEvent.bind(this),
    select:this.addEvent.bind(this),
    eventClick: this.openPopup.bind(this),
    //eventContent :this.updatetitleNew.bind(this)
    //dayCellDidMount:this.intTime.bind(this),
    //slotLabelDidMount:this.pastDays.bind(this)

  };
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;
  @ViewChild('appointModal', { static: false }) private appointModal:any;

  constructor(private router: Router,private commonservice: CommonService,private spinner: NgxSpinnerService,private modalService: NgbModal,private confirmationDialogService: ConfirmationDialogService) {}
  ngOnInit(): void {
  this.spinner.show();
  this.userDetails=JSON.parse(window.localStorage.getItem('userDetails'));
  this.commonservice.postData("", "setting/basicsetting").subscribe(res => {
  this.spinner.hide();
  if (res.status) {
  this.calSetting=res.data;
  
  this.calendarOptions.slotDuration="00:15";
  this.calendarOptions.slotLabelInterval=this.calSetting.slotDuration;
  this.calendarOptions.slotMinTime='00:00:00';
  this.calendarOptions.firstDay=this.calSetting.firstDay;
  this.calendarOptions.initialView=this.calSetting.default_mode;
  this.calendarOptions.scrollTime=this.calSetting.slotMinTime;  
  
  if(this.userDetails.user_type=='admin')
    {
     this.calendarOptions.editable=true;
     this.calendarOptions.selectable=true;
    }
    else{
      if(parseInt(this.innerWidth)<=667)
      {
        this.staff_id=this.userDetails.id;
      }
    }
    
  this.initialView=this.calSetting.default_mode;
  this.fetchStaffs();
  if(window.localStorage.getItem('selectedMoment')==null)
  {
  setTimeout(()=>{
    this.fetchAppointments();
}, 1600);  
 
  }
  else{
   this.selectedMoment=new Date(window.localStorage.getItem('selectedMoment'));
   setTimeout(()=>{
     
    this.gotoData();
}, 1600);
   
   
  
  //this.gotoData();    
  }

  }
  },
  err => {
      this.spinner.hide();
      if(err.status==401)
      {
        window.localStorage.clear();
        this.commonservice.showError("Please Login First", "Calender")
        this.router.navigate(['/']);
        

      }
      
  });

  this.commonservice.newAppointment.subscribe(data => {
  if(data!='')
  {
     let selectDate=moment(this.selectedMoment).format('YYYY-MM-DD');
     let returnDate=moment(data.appointmentDtl.start_time).format('YYYY-MM-DD');
     this.selectedMoment=new Date(data.appointmentDtl.start_time);
     const calendarApi = this.calendarComponent.getApi();
    if(selectDate==returnDate && calendarApi!=null)
    {
    
    let index = this.events.findIndex(item => item.id === parseInt(data.id));
    if(index!=-1)
    {

      this.events.splice(index, 1);
      this.events.push(data); 
      this.calendarComponent.getApi().removeAllEventSources();
      this.calendarComponent.getApi().addEventSource(this.events); 
    }
    else{
      this.events.push(data); 
      this.calendarComponent.getApi().removeAllEventSources();
      this.calendarComponent.getApi().addEventSource(this.events); //obligatory
    }
    
    
    this.calendarComponent.getApi().refetchEvents();
    this.updateTitle();
   
  }
  else{
    this.gotoData();
  }
  this.modalService.dismissAll();

  }
  });
  }

  nextAppointment():void{
  const calendarApi = this.calendarComponent.getApi();
  calendarApi.next(); // call a method on the Calendar object
  const currentDate = calendarApi.getDate();
  this.start_date=moment(currentDate).format('YYYY-MM-DD');
  window.localStorage.setItem('selectedMoment',this.start_date);
   this.selectedMoment=new Date(this.start_date);
  this.fetchAppointments();
  }

  prevAppointment():void{
  const calendarApi = this.calendarComponent.getApi();
  calendarApi.prev();
  const currentDate = calendarApi.getDate();
  this.start_date=moment(currentDate).format('YYYY-MM-DD');
  window.localStorage.setItem('selectedMoment',this.start_date);
   this.selectedMoment=new Date(this.start_date);
  this.fetchAppointments();
  }

  todayAppointment():void{
  const calendarApi = this.calendarComponent.getApi();
  calendarApi.today(); // call a method on the Calendar object
  const currentDate =calendarApi.getDate();
  this.start_date=moment(currentDate).format('YYYY-MM-DD');
  this.selectedMoment=new Date(this.start_date);
  window.localStorage.setItem('selectedMoment',this.start_date);
  this.fetchAppointments();
  }


  fetchStaffs():void{
  this.spinner.show();
  this.commonservice.postData("", "dailyStaff").subscribe(res => {
  this.spinner.hide();
  if (res.status) {
  this.staffs=res.data;
  if(parseInt(this.innerWidth)<=667)
    {
      if(this.userDetails.user_type=='admin')
      {
        this.calendarOptions.resources=this.staffs;
      }
      else{
      this.selectedStaff={id:this.userDetails.id,title:this.userDetails.name};
      this.calendarOptions.resources=[this.selectedStaff];
      }
    }
  else{
    this.calendarOptions.resources=this.staffs;
  }
  
  }
  },
  err => {
      this.spinner.hide();
      if(err.status==401)
      {
       this.router.navigate(['/unauthorized']);
      }
      this.commonservice.showError(err.message, "Calender")

  });
  }

  fetchAppointments()
  {
    this.spinner.show();
    let body={start_date:this.start_date,staff_id:this.staff_id,default_mode:this.initialView};
    this.commonservice.postData(body, "appointment/listing").subscribe(res => {
    this.spinner.hide();
    if (res.status) {
    this.events=res.data.events;
	  const calendarApi = this.calendarComponent.getApi();
    if(calendarApi!=null)
    {
    calendarApi.removeAllEventSources();
	  calendarApi.addEventSource(this.events); //obligatory
	  calendarApi.refetchEvents();
	  this.updateTitle();
    this.calHeading=res.data.heading;
    }
    }
    },
    err => {
        this.spinner.hide();
        if(err.status==401)
        {
         this.router.navigate(['/unauthorized']);
        }
        this.commonservice.showError(err.message, "Calender")

    });
  }

  changecalMode()
  {
      const calendarApi = this.calendarComponent.getApi();
      calendarApi.changeView(this.initialView); // call a method on the Calendar object
      const currentDate =calendarApi.getDate();
      if(window.localStorage.getItem('selectedMoment')==null)
      {
       this.start_date='';   
       this.fetchAppointments();
      }
      else{
       this.selectedMoment=new Date(window.localStorage.getItem('selectedMoment'));
       this.gotoData();
      }
  }
  updateTitle()
  {
    let loggedUser=this.userDetails;
     
	   $(".appointment-details-not-open").removeClass("appointment-details-not-open");
	   $(".appointment-details-moda-active").removeClass("appointment-details-moda-active");
	   $(".has-popup-open").removeClass("has-popup-open");
	   $(".no-eventtimegrid").removeClass("no-eventtimegrid");
     
     let  current=this;
     $('.fc-timegrid-slot').each(function(i, obj) {
      var hms = $(this).attr("data-time");   // your input string
      var a = hms.split(':'); // split it at the colons
      // minutes are worth 60 seconds. Hours are worth 60 minutes.
      var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 

      var hms = current.calSetting.slotMinTime;   // your input string
      var a = hms.split(':'); // split it at the colons
      // minutes are worth 60 seconds. Hours are worth 60 minutes.
      var MinTime_seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 

      var hms = current.calSetting.end_hour;   // your input string
      var a = hms.split(':'); // split it at the colons
      // minutes are worth 60 seconds. Hours are worth 60 minutes.
      var EndHour_seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
      
      if(seconds<MinTime_seconds || seconds>=EndHour_seconds)
      {
        
        $(this).addClass("week-off");
      }
  });
            
       this.events.forEach((currentValue: any) => {
       
       $(".myclass"+currentValue.id).find(".fc-event-title").html(currentValue.title+'<br>'+'<span><img src="assets/images/video-camera.png" alt="#" >&nbsp;'+currentValue.description+'</span>');
       $(".myclass"+currentValue.id).find(".fc-event-title-container").css({position:"relative"});
       

       let display="";
       let editdisplay="";
       if(loggedUser.user_type=='staff')
       {
           display="display:none;";
       }
       if(currentValue.appointmentDtl.is_recurring)
       {
        editdisplay="display:none;";
       }
       let customer_name=currentValue.appointmentDtl.name;
       if(currentValue.appointmentDtl.alternate_name!='')
       {
        customer_name+=" / "+currentValue.appointmentDtl.alternate_name;
       }
       let customer_phone=currentValue.appointmentDtl.mobile;
       if(currentValue.appointmentDtl.alternate_mobile!='')
       {
        customer_phone+=" / "+currentValue.appointmentDtl.alternate_mobile;
       }
       let customer_address=currentValue.appointmentDtl.customer_address;
       let popup='<div class="appointment-details" id="popup_id'+currentValue.id+'" style="display:none;" data-id="'+currentValue.id+'">'+

                    '<div class="appointment-details-header">'+
                     'Appointment Details <button type="button" class="close_btn">&times;</button>'+
                    '</div>'+
                    '<div class="appointment-details-body" style="height:16rem;">'+
                      '<div class="d-flex flex-wrap mb-1">'+
                        '<div class="col-sm-9 metting_start_at">'+currentValue.metting_start_at+'</div>'+
                        '<div class="col-sm-3">'+
                          '<select style="display:none;">'+
                            '<option>Label</option>'+
                          '</select>'+
                        '</div>'+
                      '</div>'+
                      '<table>'+
                        '<tbody>'+
                          '<tr>'+
                            '<td>Staff</td>'+
                            '<td id="staff_name'+currentValue.id+'">'+currentValue.staff_name+'</td>'+
                          '</tr>'+
                          '<tr>'+
                            '<td>Service</td>'+
                            '<td>'+currentValue.appointmentDtl.service_name+'</td>'+
                          '</tr>'+

                          '<tr>'+
                            '<td>Customer</td>'+
                            '<td>'+
                              '<a href="#">'+customer_name+'</a><br>'+
                              ''+customer_phone+'<br>'+
                              ''+customer_address+'<br>'+
                            '</td>'+
                          '</tr>'+
                           '<tr>'+
                              '<td>Notes</td>'+
                              '<td>'+currentValue.appointmentDtl.notes+'</td>'+

                          '</tr>'+
                          '<tr>'+
                            '<td>Job Completion Status</td>'+
                            '<td>'+currentValue.appointmentDtl.job_completion+'</td>'+
                          '</tr>'+
                          '<tr>'+
                            '<td>Extra Materials </td>'+
                            '<td>'+currentValue.appointmentDtl.extra_materials+'</td>'+
                          '</tr>'+
                          '<tr>'+
                            '<td>Summary</td>'+
                            '<td>'+currentValue.appointmentDtl.summary+'</td>'+
                          '</tr>'+
                          '<tr style="display:none;">'+
                            '<td>Booking ID</td>'+
                            '<td>'+currentValue.code+'</td>'+
                          '</tr>'+
                        '</tbody>'+
                      '</table>'+
                    '</div>'+
                    '<div class="appointment-details-footer" style="'+display+'">'+
                      '<button type="button" class="btn btn-bookAppoin edit_appointment" style="'+editdisplay+'">Edit Appointment</button>'+
                      '<button type="button" class="btn btn-cancel reschedule_appointment" data-reschedule_id="'+currentValue.id+'" style="'+editdisplay+'">Reschedule</button>'+
                      '<button type="button" class="btn btn-cancel copy_appointment">Duplicate</button>'+
                      '<button type="button" class="btn btn-cancel del_appointment">Delete</button>'+
                    '</div>'+
                    '</div>'
					
           $(".myclass"+currentValue.id).find(".appointment-details").remove();
		   
           $(".myclass"+currentValue.id).find(".fc-event-title-container").append(popup);
           $(".myclass"+currentValue.id).parents().eq(3).addClass('appointment-details-not-open');
          
          //  $(".myclass"+currentValue.id).parents().eq(0).addClass('appointment-details-moda-active');
       });
     $(".close_btn").click(function(this: HTMLButtonElement,event){
     $(this).parent().parent().hide() ;

     $(this).parents().eq(9).addClass('appointment-details-not-open');
     $(this).parents().eq(6).removeClass('appointment-details-moda-active');
     $(this).parents().eq(9).removeClass('has-popup-open');
     $(this).parents().eq(14).removeClass('appointment-details-not-open');
     $(this).parents().siblings(".fc-timegrid-col-events").removeClass('no-eventtimegrid');
     event.stopPropagation();

     })
     let self = this;
     $(".del_appointment").click(function(){
     self.deleteEvent();
     })
     $(".edit_appointment").click(function(event){
     $(this).parent().parent().hide();
     event.stopPropagation();
     self.editEvent();
     })
     $(".copy_appointment").click(function(event){
      $(this).parent().parent().hide();
      event.stopPropagation();
      self.copyEvent();
      })
      $(".reschedule_appointment").click(function(event){
        $(this).parent().parent().hide();
        event.stopPropagation();
        var reschedule_id=$(this).attr("data-reschedule_id");
        self.reschdule(reschedule_id);
        })
      
    
  }

  updateEvent(event: any)
  {
      let dayName=moment(event.event.start).format('dddd');
     
     if(event.event.extendedProps.appointmentDtl.is_recurring) {
      event.revert(); 
      this.commonservice.showError("Not Allowed for Recurring Events", "Calender") 
      const calendarApi = this.calendarComponent.getApi();
      calendarApi.refetchEvents();
      this.updateTitle(); 
     }
     else{
      
      this.spinner.show();
      let resourceId=event.event._def.resourceIds;
      let body={start_time:moment(event.event.start).format('YYYY-MM-DD HH:mm:ss'),end_time:moment(event.event.end).format('YYYY-MM-DD HH:mm:ss'),id:event.event.id,
               staff_id:resourceId[0]};
               
      this.commonservice.postData(body, "appointment/edit").subscribe(res => {
      this.spinner.hide();
      if (res.status) {
      this.commonservice.showSuccess(res.message, "Calender");
	    let index = this.events.findIndex(item => item.id === parseInt(event.event.id));
	    this.events[index]=res.data;
      const calendarApi = this.calendarComponent.getApi();
      calendarApi.removeAllEventSources();
      calendarApi.addEventSource(this.events); //obligatory
      calendarApi.refetchEvents();
      this.updateTitle();
      }
      },
      err => {
          this.spinner.hide();
          if(err.status==401)
          {
           this.router.navigate(['/unauthorized']);
          }
          this.commonservice.showError(err.message, "Calender")

      });
      }
  }
  addEvent(event: any)
  {
      $(".appointment-details").fadeOut();
      let start=moment(event.startStr);

      let end=moment(event.endStr);

      let duration=end.diff(start, 'minutes');

      let service_name=duration+' Mins Meeting';

      let weekend=moment(event.start).format('ddd');
      if(this.reschedule_id=='')
      {
      let body={duration:duration,start_time:new Date(event.start),service_name:service_name,staff_id:event.resource.id};
      
      this.commonservice.setDuration.next(body);

      this.modalService.open(this.appointModal,{ size:'lg' }).result.then((result) => {

    }, (reason) => {

       });
    }
    else{
      let body={id:this.reschedule_id,start_time:moment(event.start).format('YYYY-MM-DD HH:mm:ss'),staff_id:event.resource.id};
      this.submitReschdule(body); 
    }

  }
  reschdule(reschedule_id)
  {
     this.reschedule_id=reschedule_id;
     $("#popup_id"+this.reschedule_id+" .close_btn").trigger("click");
  } 
  submitReschdule(body)
  {
      this.spinner.show();
      this.commonservice.postData(body, "reSchedule").subscribe(res => {
      this.spinner.hide();
      if (res.status) {
      this.commonservice.showSuccess(res.message, "Calender")
      const calendarApi = this.calendarComponent.getApi();
	    let index = this.events.findIndex(item => item.id === parseInt(body.id));
      if(index!=-1)
      {
        this.events.splice(index, 1);
      }
      this.reschedule_id="";
      this.events.push(res.data);
      calendarApi.removeAllEventSources();
	    calendarApi.addEventSource(this.events); //obligatory
	    calendarApi.refetchEvents();
	    this.updateTitle();

      }
      },
      err => {
          this.spinner.hide();
          if(err.status==401)
          {
           this.router.navigate(['/unauthorized']);
          }
          this.commonservice.showError(err.message, "Calender")

      }); 






  }
  gotoData()
  {
       
          const calendarApi = this.calendarComponent.getApi();
          this.start_date=moment(this.selectedMoment).format('YYYY-MM-DD');
          if(calendarApi!=null)
          {
          calendarApi.gotoDate(this.start_date); // call a method on the Calendar object
        
          window.localStorage.setItem('selectedMoment',this.start_date);

          this.datepicker_open=false;
        
          this.fetchAppointments();
          }
  }

  openPopup(arg:any)
  {
		
      $(".appointment-details").hide();

      $(".appointment-details").parents().eq(12).addClass('appointment-details-not-open');
      $(".fc-timegrid-col-events").removeClass("no-eventtimegrid");

      this.appointmentDtl=arg.event.extendedProps.appointmentDtl;

      $(".myclass"+arg.event.id).parents().eq(3).removeClass('appointment-details-not-open');

      $(".myclass"+arg.event.id).find(".appointment-details").show();
      $(".myclass"+arg.event.id).parents().eq(0).addClass('appointment-details-moda-active');
      $(".myclass"+arg.event.id).parents().eq(3).addClass('has-popup-open');
      $(".myclass"+arg.event.id).parents().siblings(".fc-timegrid-col-events").addClass('no-eventtimegrid');
  }
  deleteEvent()
  {


      this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to Remove this... ?')

     .then((confirmed) => {

     if(confirmed) {

      this.spinner.show();

      let body={id:this.appointmentDtl.id};
	  
      this.commonservice.postData(body, "appointment/delete").subscribe(res => {

      this.spinner.hide();

      if (res.status) {

      this.commonservice.showWarning(res.message, "Calender")

      const calendarApi = this.calendarComponent.getApi();
	    let index = this.events.findIndex(item => item.id === parseInt(this.appointmentDtl.id));
	    this.events.splice(index, 1);
      calendarApi.removeAllEventSources();
	    calendarApi.addEventSource(this.events); //obligatory
	    calendarApi.refetchEvents();
	    this.updateTitle();

      }
      },
      err => {
          this.spinner.hide();
          if(err.status==401)
          {
           this.router.navigate(['/unauthorized']);
          }
          this.commonservice.showError(err.message, "Calender")

      });

     }


     })

     .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  editEvent()
  {
      this.spinner.show();

      let body={id:this.appointmentDtl.id};

      this.commonservice.postData(body, "viewAppointment").subscribe(res => {

      this.spinner.hide();

      if (res.status) {
      let currentDataSet=res.data;
      
      currentDataSet.start_time=new Date(currentDataSet.start_time);
    
      this.commonservice.setDuration.next(currentDataSet);
      this.commonservice.setNav.next(2);
      this.modalService.open(this.appointModal,{ size:'lg' }).result.then((result) => {

    }, (reason) => {

       });
      }
      },
      err => {
          this.spinner.hide();
          if(err.status==401)
          {
           this.router.navigate(['/unauthorized']);
          }
          this.commonservice.showError(err.message, "Calender")

      });
     
  }
  copyEvent()
  {
      this.spinner.show();

      let body={id:this.appointmentDtl.id};

      this.commonservice.postData(body, "viewAppointment").subscribe(res => {

      this.spinner.hide();

      if (res.status) {
      let currentDataSet=res.data;
      currentDataSet.select_staff_id=currentDataSet.staff_id;
      currentDataSet.start_time=new Date(currentDataSet.start_time);
      currentDataSet.id="";
      currentDataSet.staff_id="";
      currentDataSet.is_duplicate=true;
      this.commonservice.setDuration.next(currentDataSet);
      this.commonservice.setNav.next(2);
      this.modalService.open(this.appointModal,{ size:'lg' }).result.then((result) => {

    }, (reason) => {

       });
      }
      },
      err => {
          this.spinner.hide();
          if(err.status==401)
          {
           this.router.navigate(['/unauthorized']);
          }
          this.commonservice.showError(err.message, "Calender")

      });
     
  }

filterStaff(staff:any)
{
     this.selectedStaff=this.staffs.find(item => item.id === staff);
     
     if(this.calSetting.default_mode=='resourceTimeGrid')
     {
       if(staff!='')
       {
       this.calendarOptions.resources=[this.selectedStaff];
       }
       else{
        this.calendarOptions.resources=this.staffs;
       }
       if(window.localStorage.getItem('selectedMoment')==null)
        {
        this.fetchAppointments();
        }
        else{
        this.selectedMoment=new Date(window.localStorage.getItem('selectedMoment'));
        this.gotoData();
        }
     }
     
       
}

openmeetingModal()
{
     this.modalService.open(this.appointModal,{ size:'lg' }).result.then((result) => {

    }, (reason) => {

       });
}
redirectCustomer()
{
  window.localStorage.setItem('openPopuop',"1");
  this.router.navigateByUrl("customers");
}
redirectStaff()
{
  window.localStorage.setItem('openPopuop',"1");
  this.router.navigateByUrl("setting/staff-setting/add");
}

redirectService()
{
  window.localStorage.setItem('is_addService',"1");
  this.router.navigateByUrl("setting/services-setting");
}
refreshEvent()
{
  const calendarApi = this.calendarComponent.getApi();
  calendarApi.removeAllEventSources();
  calendarApi.addEventSource(this.events); //obligatory
	calendarApi.refetchEvents();
	this.updateTitle();
}
}
