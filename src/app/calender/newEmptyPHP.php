N=>Never,D=>Daily,M=>Monthly,W=>Weekly,Y=>Yearly,C=>Custom
git commit -m "First release of Hello World!"
git push -u origin master
<?php
if($new_appointment->custom_type=='D')
{
$start_time = Carbon::parse($new_appointment->start_time)->addDays($new_appointment->custom_duration)->format('Y-m-d H:i:s');
$end_time = Carbon::parse($new_appointment->end_time)->addDays($new_appointment->custom_duration)->format('Y-m-d H:i:s');
}
else if($new_appointment->custom_type=='W')
{
$start_time = Carbon::parse($new_appointment->start_time)->addWeeks($new_appointment->custom_duration)->format('Y-m-d H:i:s');
$end_time = Carbon::parse($new_appointment->end_time)->addWeeks($new_appointment->custom_duration)->format('Y-m-d H:i:s');
}
else if($new_appointment->custom_type=='M')
{
$start_time = Carbon::parse($new_appointment->start_time)->addMonths($new_appointment->custom_duration)->format('Y-m-d H:i:s');
$end_time = Carbon::parse($new_appointment->end_time)->addMonths($new_appointment->custom_duration)->format('Y-m-d H:i:s');
}
?>