import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { ConfirmationDialogService } from '../../confirmation-dialog/confirmation-dialog.service';
import {NgbPopover } from '@ng-bootstrap/ng-bootstrap';

import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-staff-setting',
  templateUrl: './staff-setting.component.html',
  styleUrls: ['./staff-setting.component.css']
})
export class StaffSettingComponent implements OnInit {

  customerTbaactive = 1;
  
  staffs:any=[];
  
  selected_staff:any;
  
  select_class:any;
 
  staffForm:any=FormGroup;
  
  IsSubmitted:any=false; 
  
  userDetails:any; 
   
  initialIndex:any;
  deletedStaffs:any=[];
  access_levels:any=[{key:'one_day_view',value:'View Only Day Only'},{key:'view_all',value:'View All Staff'},{key:'edit',value:'Edit And View All Staff'}]; 
  @ViewChild('popover') public popover: NgbPopover;
  constructor(private router: Router,private commonservice: CommonService,private formBuilder: FormBuilder, private spinner: NgxSpinnerService,private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit(): void {
    this.userDetails=JSON.parse(window.localStorage.getItem('userDetails'));      
    this.Loading();
    this.staffForm =  this.formBuilder.group({
      name: ["",Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ["",Validators.required],
      phone_no: [''],
      access_level:[null,Validators.required]
 
    }) 
     this.commonservice.staffEdit.subscribe(res => {
      if(res!='')  
      {
          let index = this.staffs.findIndex(item => item.id === parseInt(res.id));
          this.staffs[index].name=res.name;
          this.commonservice.staffEdit.next('');
      }
      },
      err => {
      });   
        
  }
  
  async fetchStaffs()
  {
      
       await this.commonservice.postDataAsync("", "listStaff").then(res => {
        if (res.status) {
        this.staffs=res.data;
        let openPopuop=window.localStorage.getItem('openPopuop');
        if(openPopuop!=null)
        {
          this.popover.open();
          window.localStorage.removeItem("openPopuop"); 
        }
        }
      },
        err => {
            this.spinner.hide();
            if(err.status==401)
            {
              window.localStorage.clear();
              this.commonservice.showError("Please Login First", "Staff")
              this.router.navigate(['/']);
            }  
              

        }); 
  }
  async Loading()
 {
   this.spinner.show();
   await this.fetchStaffs();
   this.selected_staff=this.staffs[0];
   this.commonservice.viewStaff.next(this.selected_staff);
   this.spinner.hide();
 }
  staffDetail(staff:any)
  {
      this.selected_staff=staff;
      this.commonservice.viewStaff.next(staff);
  }
  async addStaff(pop:any)
  {
      this.IsSubmitted=true; 
      
      if (this.staffForm.status == "VALID") {
     
      this.spinner.show(); 
      
      await this.commonservice.postDataAsync(this.staffForm.value, "addStaff").then(res => {
          
      this.spinner.hide();
        
         
      if (res.status) {
          
      this.commonservice.showSuccess(res.message, "Staff");
      
      this.IsSubmitted=false;
      
      this.commonservice.staffEdit.next({id:res.data.id,name:res.data.name});
      this.staffForm.reset(); 
       pop.close()  ;
      
      
      }
      else{
        let errors=res.error;
        
        for (let error of errors) 
        {
         this.commonservice.showError(error, "Staff")
        }
       }
      },
        err => {
            this.spinner.hide(); 
            if(err.status==401)
            {
             this.router.navigate(['/unauthorized']); 
            }  
            this.commonservice.showError(err.message, "Staff")    

        });
   await this.fetchStaffs();
   this.selected_staff=this.staffs[0];
   this.commonservice.viewStaff.next(this.selected_staff);
   this.spinner.hide();    
        
     
 }
 }
 deleteData(id:any='',event:any='')
 {
     event.stopPropagation(); 
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to Remove this... ?')
      
     .then((confirmed) => {
         
     if(confirmed) { 
          
      this.spinner.show();
       
      let body={ids:id};
      
     this.commonservice.postData(body, "deleteStaff").subscribe(res => {
          
      this.spinner.hide();
      
      if (res.status) {
          
      this.commonservice.showError(res.message, "Staff")
     
      this.Loading();
      
      }
      },
      err => {
          this.spinner.hide();
          if(err.status==401)
          {
           this.router.navigate(['/unauthorized']);
          }
          this.commonservice.showError(err.message, "Calender")

      });
      
            
     }    
     
     
     })
   
     .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
 }     

  
   drop(i:any) {
   this.moveElement(i); 
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(i:any) {
  this.initialIndex=i;  
  }
 moveElement(finalIndex:any) {
	this.staffs.splice(finalIndex,0,this.staffs.splice(this.initialIndex,1)[0]);
        let items=[];
	this.staffs.forEach(item=>{
        items.push(item.id);
        
    })
    
    this.spinner.show();
       
      let body={staffs:items};
      
     this.commonservice.postData(body, "sortOrder").subscribe(res => {
          
      this.spinner.hide();
      
      if (res.status) {
        this.selected_staff=this.staffs[0];
        this.commonservice.viewStaff.next(this.selected_staff);    
      
      
      }
      },
      err => {
          this.spinner.hide();
          if(err.status==401)
          {
           this.router.navigate(['/unauthorized']);
          }
          this.commonservice.showError(err.message, "Calender")

      });
 	
}

assignValue(staff:any,e:any):void{
   
  if(e.target.checked)
  {
      this.deletedStaffs.push(staff.id);
      
  }
  else{
      let index=this.deletedStaffs.indexOf(staff.id);
      this.deletedStaffs.splice(index, 1);
      
  }
  console.log(this.deletedStaffs);
  e.stopPropagation();
   
  }
  deleteAll()
  {
    if(this.deletedStaffs.length==0)
    {
      this.commonservice.showError("Please select atleast one staff", "Staff");
    }
    else{
      this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to Remove All... ?')
      
      .then((confirmed) => {
          
      if(confirmed) { 
           
       this.spinner.show();
        
       let body={ids:this.deletedStaffs};
       
      this.commonservice.postData(body, "deleteStaff").subscribe(res => {
           
       this.spinner.hide();
       
       if (res.status) {
           
       this.commonservice.showError(res.message, "Staff")
      
       this.Loading();
       
       }
       },
       err => {
           this.spinner.hide();
           if(err.status==401)
           {
            this.router.navigate(['/unauthorized']);
           }
           this.commonservice.showError(err.message, "Calender")
 
       });
       
             
      }    
      
      
      })
    
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));  
    }  
    
  }
}
