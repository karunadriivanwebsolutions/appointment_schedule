<div class="modal-header book-apoin-header">
    <h4 class="modal-title" id="modal-basic-title">Merge {{deletedClients.length}} Customers</h4>
    <button type="button" class="close" aria-label="Close" (click)="modal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body book-apoin-body">
    <form method="post" [formGroup]="mergeForm" (ngSubmit)="mergeData()" *ngIf="mergeForm">
      <div class="row">
        <div class="form-group mb-3">
          <label><img src="assets/images/default-user-2.png"></label>
          <ng-select formControlName="name">
           <ng-option *ngFor="let user of names"  [value]="user">{{user}}</ng-option>
           </ng-select>                      
       </div>
       
       <div class="form-group mb-3">
         <label>Primary Contact Name</label>
         <ng-select formControlName="primary_contact_name">
          <ng-option *ngFor="let primary_contact of primary_contacts"  [value]="primary_contact">{{primary_contact}}</ng-option>
         </ng-select>   
         <ul class="parsley-errors-list filled" aria-hidden="false" *ngIf="mergeForm.get('primary_contact').invalid && MergeSubmitted">
 
           <li class="parsley-required">Please Select Primary Contact Name</li>
     </ul>
      </div>
      <div class="form-group mb-3">
       <label>Primary Mobile No</label>
       <ng-select formControlName="mobile">
        <ng-option *ngFor="let primary_mobile of mobiles"  [value]="primary_mobile">{{primary_mobile}}</ng-option>
       </ng-select>   
        
    </div>
      <div class="form-group mb-3">
       <label>Alternate Contact Name</label>
       <ng-select formControlName="alternate_name">
        <ng-option *ngFor="let val of alternate_names"  [value]="val">{{val}}</ng-option>
       </ng-select>   
      
    </div>
    <div class="form-group mb-3">
     <label>Alternate Mobile No</label>
     <ng-select formControlName="alternate_mobile">
      <ng-option *ngFor="let val of alternate_mobiles"  [value]="val">{{val}}</ng-option>
     </ng-select>   
    
    </div>
    <div class="form-group mb-3">
     <label>Adrress</label>
     <ng-select formControlName="address">
      <ng-option *ngFor="let val of addresses"  [value]="val">{{val}}</ng-option>
     </ng-select>   
    
    </div>
    <div class="form-group">
     
    <input type="submit" class="btn btn-bookAppoin w-100" value="Merge {{deletedClients.length}} Customers" >
 </div>  


      </div>  
          
      </form> 