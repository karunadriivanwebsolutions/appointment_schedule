import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {
  appointments:any=[];
  
  constructor(private modalService: NgbModal,private router: Router,private commonservice: CommonService,private spinner: NgxSpinnerService) { }

  closeResult = '';
  
  selected_customer:any;

  ngOnInit(): void {
      
  this.commonservice.newCustomer.subscribe(customer => {
  if(customer.id!='')
  {    
  this.currentweekAppointments(customer.id);
  this.selected_customer=customer;
  
  }     
  });      
  this.commonservice.afterAppointment.subscribe(customer_id => {
  if(customer_id!='')
  {    
  this.currentweekAppointments(customer_id);
  this.modalService.dismissAll();
  
  }     
  });         
  }
  open(content: any) {
    let body=  this.selected_customer;
    body.customer_notes=body['notes'];
    delete body['notes'];
    this.commonservice.presentCustomer.next(this.selected_customer);
    this.commonservice.setNav.next(2);   
    this.commonservice.customerTab.next(false);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      
    }, (reason) => {
       this.commonservice.customerTab.next(true);
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
currentweekAppointments(customer_id:any='')
  {
     if(customer_id!='')
     {
      this.spinner.show(); 
      let body={customer_id:customer_id};
      this.commonservice.postData(body, "appointment/dashboard").subscribe(res => {
      this.spinner.hide();  
      if (res.status) {
      this.appointments=res.data.appointments;
 
     }
   },
     err => {
         this.spinner.hide();
         if(err.status==401)
         {
          this.router.navigate(['/unauthorized']); 
         }  
         this.commonservice.showError(err.message, "Dashboard")    
 
     });             
   }
     }
     
}
