import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,BehaviorSubject } from 'rxjs';
import{ apiURL,siteUrl } from '../global-constants';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  token: any;
  constructor(private http: HttpClient,private toastr: ToastrService) { }
  public presentCustomer = new BehaviorSubject<any>('');
  public newAppointment = new BehaviorSubject<any>('');
  public newCustomer = new BehaviorSubject<any>('');
  public setDuration = new BehaviorSubject<any>('');
  public setNav = new BehaviorSubject<any>(1);
  public customerTab = new BehaviorSubject<any>(true);
  public  viewStaff= new BehaviorSubject<any>('');
  public  afterAppointment= new BehaviorSubject<any>('');
  public  staffEdit= new BehaviorSubject<any>('');
postData(body: any, url: any): Observable<any> {
    this.token = window.localStorage.getItem('authtoken');
    if (this.token == null) {
      this.token = "";
    }
    const webservice_path = apiURL;
    let form_data = new FormData();

    for (var key in body) {
      form_data.append(key, body[key]);
    }
    let httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.token
      })
    }
    return this.http.post(webservice_path + url, form_data, httpOptions)
  }

  postDataRaw(body: any, url: any): Observable<any> {
    this.token = window.localStorage.getItem('authtoken');
    if (this.token == null) {
      this.token = "";
    }
    const webservice_path = apiURL;

    let httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Authorization': this.token
      })
    }
    return this.http.post(webservice_path + url, body, httpOptions)
  }
  
  async postDataAsync(body: any, url: any): Promise<any> {
    this.token = window.localStorage.getItem('authtoken');
    if (this.token == null) {
      this.token = "";
    }
    const webservice_path = apiURL;
    let form_data = new FormData();

    for (var key in body) {
      form_data.append(key, body[key]);
    }
    let httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.token
      })
    }
    return await  this.http.post(webservice_path + url, form_data, httpOptions).toPromise()
  }

  
  showSuccess(message:any, title:any){
    this.toastr.success(message, title,{timeOut: 1000})
}

showError(message:any, title:any){
    this.toastr.error(message, title,{timeOut: 1000})
}

showInfo(message:any, title:any){
    this.toastr.info(message, title,{timeOut: 1000})
}

showWarning(message:any, title:any){
    this.toastr.warning(message, title,{timeOut: 1000})
}
  
  
}
